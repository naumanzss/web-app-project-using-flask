import flask
from flask import abort, jsonify, request, url_for
import json
import os.path
from os import path
# from flask_api import FlaskAPI, status, exceptions
# from flask import render_template

app = flask.Flask(__name__)

# Suggested internal data structure (example)
quotes = {
  "AAPL": {
    "name": "Apple", 
    "quotes": {
      "20201031-083000": [ "110.5", "111.17", "110.64", "110.64", "5684400"], 
      "20201031-083500": [ "111.5", "111.17", "111.64", "111.64", "1473400"], 
      "20201031-084000": [ "112.5", "112.17", "112.64", "112.64", "2987600"]
    }
  }, 
  "AMZN": {
    "name": "Amazon", "quotes": {}
  }
}

# quotes = {
#     'AAPL': {'name': 'Apple', 'quotes': {}}, 
#     'GOOG': {'name': 'Alphabet', 'quotes':{}},
#     'AMZN': {'name': 'Amazon', 'quotes': {}},
#     'NFLX': {'name': 'Netflix', 'quotes': {}},
#     'MSFT': {'name': 'Microsoft', 'quotes': {}}
# }

@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"

def Convert(lst):
    it = iter(lst)
    res_dct = dict(zip(it, it))
    return (res_dct)

# Return a list of all tickers and their name. E.g.:
# {
#     'AAPL': 'Apple', 
#     'GOOG': 'Alphabet',
# }   
@app.route('/tickers', methods=['GET'])
def get_tickers():
    dict1 = {}
    for key in quotes.keys():
        dict1[key] = quotes[key]['name']

    return dict1

# Return a ticker and it name. E.g.:
# {
#     'AAPL': 'Apple', 
# }  
@app.route('/ticker/<tickr>', methods=['GET'])
def get_ticker(tickr):
    dict1 = {}
    dict1[tickr] = quotes[tickr]['name']

    return dict1   

# Create a new ticker. E.g.:
# curl -i -X POST http://localhost:5000/ticker -d "ticker=CSCO&name=Cisco"
# ---
@app.route('/ticker', methods=['PUT','POST'])
def create_ticker():
    if request.method == 'POST' and request.url == "http://localhost:5000/ticker":
        ticker = (request.get_data().decode("utf8"))
        ticker = ticker.split("&")
        name = ticker[0].split("=")
        ticker = ticker[1].split("=")
        name = name[1]
        ticker = ticker[1]

        quote = {
           ticker: {'name': name , 'quotes': {}}
        }
        quotes.update(quote)

    return "Done"   

# Delete a ticker, along with all quote information. E.g.:
# curl -i  -X DELETE http://localhost:5000/ticker/MSFT
# ---
@app.route('/ticker/<tickr>', methods=['DELETE'])
def delete_ticker(tickr):
    if request.method == 'DELETE':
        ticker = (request.url)
        ticker = ticker.split("/")
        delete_ticker = (ticker[-1])
        print(delete_ticker)
        if delete_ticker in quotes.keys():
            del quotes[delete_ticker]
            return "Done"

    return "Key Doesn't Exist"  

# Get all quotes for a ticker. E.g.:
# {
#    "20201031-083000": [ "110.5", "111.17","110.64", "110.64", "5684400"], 
#    "20201031-083500": [ "111.5", "112.17", "111.64", "111.64", "5684400"], 
#    "20201031-084000": [ "112.5", "112.17", "112.64", "112.64", "5684400"]
# }
@app.route('/quotes/<tickr>',methods=['GET'])
def get_quotes(tickr):
    
    dict1 = {}
    dict1[tickr] = quotes[tickr]['quotes']
    return dict1
        
# Return a specific quote per ticker/datetime
# curl http://localhost:5000/quote/AAPL/20201031-083000
# ---
@app.route('/quote/<tickr>/<datetime>')
def get_quote(tickr,datetime):

    if "http://localhost:5000/quote" in request.url:
        ticker = (request.url)
        ticker = ticker.split("/")
        print(ticker)
        tick = ticker[-2]
        time_date = ticker[-1]
        print(time_date)
        print(quotes[tick]['quotes'][time_date])
        s = quotes[tick]['quotes'][time_date]
        listToStr = ' '.join(map(str, s)) 

    return listToStr


# Create a new quote
# curl -i -X POST http://localhost:5000/quote -d "ticker=AAPL&date=20201031&time=084000&open=110.5&high=111.17&low=110.64&close=110.64&vol=5684400"
#       
# ---
@app.route('/quote',methods=['POST'])
def add_quote():
    if request.method == 'POST' and request.url == "http://localhost:5000/quote":
        ticker = (request.get_data().decode("utf8"))
        ticker = ticker.split("&")
        tick = ticker[0].split("=")
        date = ticker[1].split("=")
        time = ticker[2].split("=")
        open = ticker[3].split("=")
        high = ticker[4].split("=")
        low = ticker[5].split("=")
        close = ticker[6].split("=")
        vol = ticker[7].split("=")
        print(ticker)
        date_time = date[1]+"-"+time[1]
        qoute_dic = {
            date_time: [open[1],high[1],low[1],close[1], vol[1]]
        }
        print(qoute_dic)
        print(type(qoute_dic))
        quotes[tick[1]]['quotes'].update(qoute_dic)
        return ("TASK done")
    return ("FAILED")
    
# --- average computation ---
# curl -i http://localhost:5000/stat/avg/AAPL/20201031-083000/20
# ---
@app.route('/stat/avg/<tickr>/<datetime>/<int:period>')
def avg(tickr, datetime, period):
    list_avg =[]
    sum_close_value = 0.0

    if "http://localhost:5000/stat/avg" in request.url:
        # ticker = (request.url)
        # ticker = ticker.split("/")
        # tick = ticker[-3]
        # date_time = ticker[-2]
        # period = ticker[-1]
        for date, value in quotes[tickr]["quotes"].items():
            list_avg.append(value)
            if datetime == date:
                break
        print(tickr, datetime, period)
        print("list_avg" ,list_avg)
        for i in list_avg:
            if(len(list_avg) <= 20):
                break
            del list_avg[0]
        
        for close_value in list_avg:
            close_value = close_value[-2]
            sum_close_value += float(close_value)

        avg = str(sum_close_value/len(list_avg))
    return ("Average = " + avg + "\nPeriod requested = " + str(period) + "\nPeriod Available = " + str(len(list_avg)))


# --- average computation ---
# curl -i http://localhost:5000/stat/atr/AAPL/20201031-083000/20
# ---
@app.route('/stat/atr/<tickr>/<datetime>/<int:period>')
def atr(tickr, datetime, period):
    list_avg = []
    sum_atr_value = 0.0

    
    if "http://localhost:5000/stat/atr" in request.url:
        for date, value in quotes[tickr]["quotes"].items():
            list_avg.append(value)
            if datetime == date:
                break
        print(tickr, datetime, period)
        print("list_avg" ,list_avg)
        for i in list_avg:
            if(len(list_avg) <= 20):
                break
            del list_avg[0]
        
        for close_value in list_avg:
            print(float(close_value[-4]) - float(close_value[-3]))
            close_value = float(close_value[-4]) - float(close_value[-3])
            sum_atr_value += (close_value)

        avg = str(sum_atr_value/len(list_avg))
    return ("Average true range = " + avg + "\nPeriod requested = " + str(period) + "\nPeriod Available = " + str(len(list_avg)))

# add CSV file to qoute
# curl -i http://localhost:5000/addfile/amzn.us.txt
# ---
@app.route('/addfile/<name_of_file>')
def addfile(name_of_file):
    if "http://localhost:5000/addfile" in request.url:
        print(name_of_file)
        if path.exists(name_of_file):
            f = open(name_of_file, "r")
            f.readline()
            content = f.read().split("\n")
            for line in content:
                if line.rstrip():
                    currentline= line.split(",")
                    print(currentline)
                    date = currentline[2]
                    time = currentline[3]
                    open1 = currentline[4]
                    high = currentline[5]
                    low = currentline[6]
                    close = currentline[7]
                    vol = currentline[8]
                    tick = line.split(".")
                    tick = tick[0]
                    date_time = date+"-"+time
                    qoute_dic = {
                        date_time: [open1,high,low,close, vol]
                        }

                    if tick not in quotes:
                        qoute_dic = {
                                tick: {'name': tick , 'quotes': {date_time: [open1,high,low,close, vol]}}
                                }
                        quotes.update(qoute_dic)
                    else:
                        quotes[tick]['quotes'].update(qoute_dic)


    return request.url

# Debug interface: return internal data structures
@app.route('/debug/dump')
def dump():
    return jsonify(quotes)   















