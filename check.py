import json


quotes = {
    'AAPL': {'name': 'Apple', 'quotes': {}}, 
    'GOOG': {'name': 'Alphabet', 'quotes':{}},
    'AMZN': {'name': 'Amazon', 'quotes': {}},
    'NFLX': {'name': 'Netflix', 'quotes': {}},
    'MSFT': {'name': 'Microsoft', 'quotes': {}}
}

# print("Type:", type(quotes)) 

def Convert(lst):
    it = iter(lst)
    res_dct = dict(zip(it, it))
    return (res_dct)
#     res_dct = dict(it)
#     print(res_dct)
#     return res_dct

list_tickers = []
for key in quotes.keys():
        # print(key, end=": ")
        # print( quotes[key]['name'])
        list_tickers.append(key + ": " )
        list_tickers.append(quotes[key]['name'])

print(list_tickers[1])
list1 = Convert(list_tickers)

# print(list1.get(1))
# json_object = json.load(quotes)

# print(json_object)
